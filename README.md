![alternativetext](https://bitbucket.org/bluehatzach/blender-beginner-levels/raw/HEAD/Images/BlenderLogo.png)

# Blender Beginner Levels

This repository is for Blender files and projects designed and developed by BlueHatZach to help introduce new users into understanding and retaining Blender better and faster.

## [Introduction](Docs/Introduction.md)

- [First Time Setup](Docs/FirstTimeSetup.md)

## [Levels](Levels/Levels.md)

- Teaching Blender as a one might design a game tutorial: incremental and participatory


## [Models](Models/Models.md)

- Benchmark tasks you complete yourself from scratch to help you master the basics of Blender


---

_If you just want my one page handy guide that all this started from, you can download it here :D [Blender First Time sheet](https://bitbucket.org/bluehatzach/blender-beginner-levels/raw/HEAD/Docs/Blender%20-%20First%20Time%20Sheet.pdf)_
