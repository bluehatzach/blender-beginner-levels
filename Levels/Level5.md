[Home](https://bitbucket.org/bluehatzach/blender-beginner-levels/src/HEAD/README.md) / [Levels](https://bitbucket.org/bluehatzach/blender-beginner-levels/src/HEAD/Levels/Levels.md) /
## Level 5 - Color Changing

[Download File](https://bitbucket.org/bluehatzach/blender-beginner-levels/raw/HEAD/Levels/Level5.blend)

The goal of this level is to:

1. Learn to modify and create colors.

Directions:
- Make the puzzle pieces 4 different colors so you can distinguish them from one another
- 2 colors are created, you have to add them to the the object, create a 3rd from scratch

#### Shortcuts used:
- Select (Right Click, A, B, C)
