[Home](https://bitbucket.org/bluehatzach/blender-beginner-levels/src/HEAD/README.md) / 
## Levels

The goal of these lessons is to introduce aspects of Blender to you piece by piece.

- [Level 1 - Select and Grab](Level1.md)
- [Level 2 - Select and Rotate](Level2.md)
- [Level 3 - Select and Scale](Level3.md)
- [Level 4 - Camera Movement](Level4.md)
- [Level 5 - Color Changing](Level5.md)
- _[Model 1 - Snow Man](https://bitbucket.org/bluehatzach/blender-beginner-levels/src/HEAD/Models/Model1.md)_
- [Level 6 - Edit Mode Select and Grab](Level6.md)
- [Level 7 - Extrude and Grab](Level7.md)
- _[Model 2 - Christmas Tree](https://bitbucket.org/bluehatzach/blender-beginner-levels/src/HEAD/Models/Model2.md)_
- _[Model 3 - Chess Pawn](https://bitbucket.org/bluehatzach/blender-beginner-levels/src/HEAD/Models/Model3.md)_


_More to come eventually_