[Home](https://bitbucket.org/bluehatzach/blender-beginner-levels/src/HEAD/README.md) / [Levels](https://bitbucket.org/bluehatzach/blender-beginner-levels/src/HEAD/Levels/Levels.md) /
## Level 6 - Edit Mode Select and Grab

[Download File](https://bitbucket.org/bluehatzach/blender-beginner-levels/raw/HEAD/Levels/Level6.blend)

The goal of this level is to:

1. Learn how to edit vertices.
2. Learn that your standard commands all still apply in Edit Mode, too.

Directions:
- Make the shape back into a cube.

#### Shortcuts used:
- Select (Right Click, A, B, C)
- Edit Mode (Tab)
- Constrain movements to grid (Control: hold after command (example: G then Control))
