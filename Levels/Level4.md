[Home](https://bitbucket.org/bluehatzach/blender-beginner-levels/src/HEAD/README.md) / [Levels](https://bitbucket.org/bluehatzach/blender-beginner-levels/src/HEAD/Levels/Levels.md) /
## Level 4 - Flying Camera

[Download File](https://bitbucket.org/bluehatzach/blender-beginner-levels/raw/HEAD/Levels/Level4Fly.blend)

The goal of this level is to:

1. Manipulate the Camera.
2. Learn some basics of rendering.

#### Shortcuts used:
- Shift + F (Fly)
- Camera view (0 or NumPad_0)
- Render Camera View (F12)
- Set this view as Camera (Ctrl + Alt + 0)
