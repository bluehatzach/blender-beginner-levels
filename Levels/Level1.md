[Home](https://bitbucket.org/bluehatzach/blender-beginner-levels/src/HEAD/README.md) / [Levels](https://bitbucket.org/bluehatzach/blender-beginner-levels/src/HEAD/Levels/Levels.md) /
## Level 1 - Puzzle 1

[Download File](https://bitbucket.org/bluehatzach/blender-beginner-levels/raw/HEAD/Levels/Level1.blend)

The goal of this level is to:

1. Select the pieces.
2. Move them into place to solve the puzzle.

#### Shortcuts used:
- Select (Right Click, A, B, C)
- Grab (G)
- Constrain movements to grid (Control: hold after command (example: G then Control))
