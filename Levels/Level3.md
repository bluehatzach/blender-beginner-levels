[Home](https://bitbucket.org/bluehatzach/blender-beginner-levels/src/HEAD/README.md) / [Levels](https://bitbucket.org/bluehatzach/blender-beginner-levels/src/HEAD/Levels/Levels.md) /
## Level 3 - Puzzle 3

[Download File](https://bitbucket.org/bluehatzach/blender-beginner-levels/raw/HEAD/Levels/Level3.blend)

The goal of this level is to:

1. Select the pieces.
2. Move and adjust their scale in order to solve the puzzle.

#### Shortcuts used:
- Select (Right Click, A, B, C)
- Grab(G)
- Scale(S)
- Constrain movements to grid (Control: hold after command (example: S then Control))
