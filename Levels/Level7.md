[Home](https://bitbucket.org/bluehatzach/blender-beginner-levels/src/HEAD/README.md) / [Levels](https://bitbucket.org/bluehatzach/blender-beginner-levels/src/HEAD/Levels/Levels.md) /
## Level 7 - Extrude and Grab

[Download File](https://bitbucket.org/bluehatzach/blender-beginner-levels/raw/HEAD/Levels/Level7.blend)

The goal of this level is to:

1. Practice Extrusion on simple shapes.
2. Copy a pattern given to you.

Directions:
- Duplicate the letter to the right by extruding the cube given to you.

Tips:
- Watch out for duplicate vertices and internal faces.

#### Shortcuts used:
- Select (Right Click, A, B, C)
- Edit Mode (Tab)
- Extrusion (E)
- Remove duplicate vertices / Remove doubles (W)
- Delete Faces (X)
