[Home](https://bitbucket.org/bluehatzach/blender-beginner-levels/src/HEAD/README.md) / [Models](https://bitbucket.org/bluehatzach/blender-beginner-levels/src/HEAD/Models/Models.md) /
## Model 1 - Snow Man

The goal of this model is to:

1. Practice navigating in 3D space
2. Moving, rotating, and scaling objects.

![alternativetext](https://bitbucket.org/bluehatzach/blender-beginner-levels/raw/HEAD/Images/Models/Model1/SnowManRender.png)

Suggested basic approach:

1. Add a sphere for base
2. Duplicate it, move it up, scale it smaller
3. Repeat step 2 for a head
4. Add a cylindar for the arm
5. Make it thin and long like a twig
6. Rotate it and move it to an arm position
7. Duplicate for the other side.
8. Add a sphere for the eyes, make small, position, and duplicate.
9. Add a cone for a nose, scale, rotate, and position
10. Add and apply colors

![alternativetext](https://bitbucket.org/bluehatzach/blender-beginner-levels/raw/HEAD/Images/Models/Model1/SnowManWireframe.png)

#### Shortcuts used:
- Select (Right Click, A, B, C)
- Grab(G), Rotate(R), Scale(S)
- Constrain movements to axis (example: G then tap X/Y/Z; left click to confirm, right click to cancel)
- Shift A to Add
- Shift C to Center cursor

