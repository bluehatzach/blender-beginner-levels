[Home](https://bitbucket.org/bluehatzach/blender-beginner-levels/src/HEAD/README.md) /
## Models

Each of these Models will be created by you from scratch based off of the skills you have already learned.

- [Model 1 - Snow Man](Model1.md) (Shift A to Add, Shift C to Center cursor, GRS, Colors)
- [Model 2 - Christmas Tree](Model2.md) (Extrude and scale, colors)
- [Model 3 - Chess Pawn](Model3.md) (Extrude and scale, Basic Texturing (Map from view))

Challenge Suggestions

- Challenge Model 4 - Chess Board (Apply Texture to Selection)
- Challenge Model 5 - Build a table for the chess board (UV unwrap texture)

