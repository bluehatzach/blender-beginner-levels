[Home](https://bitbucket.org/bluehatzach/blender-beginner-levels/src/HEAD/README.md) / [Models](https://bitbucket.org/bluehatzach/blender-beginner-levels/src/HEAD/Models/Models.md) /
## Model 3 - Chess Pawn

The goal of this level is to:

1. Model a chess pawn starting from a cylinder/circle using Extrusion.
2. Learn Loop Cuts and Loop Selects.
3. Understand basics of textures and UV maps in Blender.

[Reference Image](https://upload.wikimedia.org/wikipedia/commons/3/3d/StauntonPawn2.jpg)

Suggestions for textures

- [Light Wood](https://www.pexels.com/photo/abstract-backdrop-background-board-129733/)
- [Medium Wood](https://www.pexels.com/photo/abstract-antique-background-board-172276/)

#### Shortcuts used:
- Select (Right Click, A, B, C)
- Loop Select (Alt + Right Click on an edge)
- Loop Cut (Ctrl + R then Left Click on an edge)
- Grab(G), Rotate(R), Scale(S)
- Constrain movements to axis (example: G then tap X/Y/Z; left click to confirm, right click to cancel)
- Shift A to Add
- Shift C to Center cursor
- Make Face (F)
- Fill Faces (Shift + F)

![alternativetext](https://bitbucket.org/bluehatzach/blender-beginner-levels/raw/HEAD/Images/Models/Model3/ChessPawn.png)