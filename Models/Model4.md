## Level 1 - Puzzle 1

The goal of this level is to:
1. Select the pieces
2. Move them into place to solve the puzzle.

#### Shortcuts used:
- Select (Right Click, A, B, C)
- Grab (G)
- Constrain movements to grid (Control: hold after command (example: G then Control))
