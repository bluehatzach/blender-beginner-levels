[Home](https://bitbucket.org/bluehatzach/blender-beginner-levels/src/HEAD/README.md) / [Models](https://bitbucket.org/bluehatzach/blender-beginner-levels/src/HEAD/Models/Models.md) /
## Model 2 - Christmas Tree

The goal of this level is to:

1. Select the pieces
2. Move them into place to solve the puzzle.

Tips
- You can hit (E)xtrude and then, instead of Left Click to place the selection where you moved it to, use Right Click to place the selection exactly on top of the points you extruded from. Then you can (S)cale the selection and it scales on the same plane as the oriiginal selection. That's alot of words for try it and see how it goes, its a great technique.

#### New Shortcuts used:
- Extrude (E)

![alternativetext](https://bitbucket.org/bluehatzach/blender-beginner-levels/raw/HEAD/Images/Models/Model2/TreeRender.png)

#### How I got the render to look this way (Try rendering them one at a time to see the effect each has)

- Made Point lamp into Sun lamp
- Changed the Green's specularity intensity to 0
- Changed the Ambient Occlusion to 0.5
