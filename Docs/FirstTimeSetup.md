[Home](https://bitbucket.org/bluehatzach/blender-beginner-levels/src/HEAD/README.md) /
## First Time Setup

If this is your first time using Blender, please go through and make these modifications. They are very useful and offer a bit more utility, especially for laptop users. Even if you're not using a laptop, I highly suggest (and if you are in my workshop today, require) you to make these changes.

1. Under File > User Preferences > Input
	- Enable – Emulate 3 button mouse
	- Enable – Emulate Numpad (Makes ‘Alt + Left Click’ = Middle Mouse)
	
	![alternativetext](https://bitbucket.org/bluehatzach/blender-beginner-levels/raw/HEAD/Images/Setup/InputEmulate.png)


2. Turn on XRay View by going to Edit mode (with mouse over 3D viewport, hit Tab) > Disable Limit Selection Buffer Button ; hit tab again to return to object mode.

	![alternativetext](https://bitbucket.org/bluehatzach/blender-beginner-levels/raw/HEAD/Images/Setup/XRayView.png)


3. Save the startup file (the default file that loads whenever you load Blender) by going to: File > Save Startup File

	![alternativetext](https://bitbucket.org/bluehatzach/blender-beginner-levels/raw/HEAD/Images/Setup/StartUpFile.png)
	
	