[Home](https://bitbucket.org/bluehatzach/blender-beginner-levels/src/HEAD/README.md) / 
# Introduction

## No good place to start
These Blender projects were designed primarily as a means actively of engaging users. Blender introductions are traditionally a massive information dump - so many different ideas and shortcuts need to be explained that it is incredibly overwhelming.

As I thought through this, I realized the problem was like the tutorial levels on a video game. I have a game myself that is in development (https://gamejolt.com/games/rest-your-eyes/186178) that, at the time of writing, has a tutorial that is all text and sound effect examples with no chance for the user to try any of the concepts I was attempting to explain. And yes, this is terrible design. Tutorials should invite users to partake, not to sit passively. They should be slow to scale but not be demeaning to those already familiar with the game. So I realized as I prepared for a presentation one day - what makes Blender any different?

So in an effort to make my talk more engaging like a game and less like a lecture, I created some Blender projects that would allow for stepping stones to split up the large information overload required to understand the simplest basics necessary for Blender.

### Blender Versions
As of 2019/03/14, this tutorial series covers the basics of Blender 2.79 - 2.5. While many features where introduced during these years of Blender, the core experience for beginners changed very little, if at all.

_Note: Classroom based
This tutorial series is designed to be used in the classroom. You are welcome to use it on your own, but the purpose is its use as a learning supplement with hands on teaching. This might change at a later date (I hope it will) as I work on expanding this; but for now, if it feels like there are missing pieces, this is why._